

-- 2.a.	Find all artists that has letter D in their name
SELECT * FROM artists WHERE name LIKE "%d%";

-- 2.b.	Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230;

-- 2.c.	Join the 'albums' and 'songs' tables (only show the album name, song name, and song length)
SELECT album_title AS "album name", song_name AS "song name", length AS "song length" FROM albums JOIN songs ON albums.id = songs.album_id;

-- 2.d.	Join the 'artists' and 'albums' tables (find all albums that has letter A in its name)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- 2.e.	Sort the albums in Z-A order (show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 2.f.	Join the 'albums' and 'songs' tables (sort albums from Z-A and sort songs from A-Z)
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY song_name ASC;
